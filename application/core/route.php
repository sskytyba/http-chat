<?php

namespace core;

define('GLOBAL_APP_ROOT', $_SERVER["DOCUMENT_ROOT"]);

class Route
{
    static function start()
    {
        session_start();

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if(!empty($routes[1]) && !empty($routes[2]))
        {
            Route::redirect($routes[1],$routes[2]);
        }
        else if(!empty($routes[1]))
        {
            Route::redirect($routes[1]);
        }
        else
        {
            Route::redirect();
        }
    }

    static function redirect($controller = 'Login', $action = 'index') // default controller and action
    {
        // pull out controller name
        if ( !empty($controller) )
        {
            $controller_name = $controller;
        }
        else
        {
            // we can throw an exception here too.
            Route::ErrorPage404();
            return;
        }

        // pull out action name
        if ( !empty($action) )
        {
            $action_name = $action;
        }
        else
        {
            // we can throw an exception here too.
            Route::ErrorPage404();
            return;
        }

        // pick up the file with the model class (model file may not be present)

        $model_file = strtolower($controller_name).'_model.php';
        $model_path = GLOBAL_APP_ROOT."/application/models/".$model_file;

        if(file_exists($model_path))
        {
            include $model_path;
        }

        // pick up the file with the controller class
        $controller_file = strtolower($controller_name).'_controller.php';
        $controller_path = GLOBAL_APP_ROOT."/application/controllers/".$controller_file;

        if(file_exists($controller_path))
        {
            include $controller_path;
        }
        else
        {
            /*
            it would be correct to throw an exception here,
             but for simplicity we just do a redirection to the page404.
            */
            Route::ErrorPage404();
            return;
        }

        // add prefixes
        $controller_name = 'Controller'.$controller_name;
        $action_name = 'action_'.$action_name;

        // create controller here
        $controller = new $controller_name;
        $action = $action_name;

        if(method_exists($controller, $action))
        {
            // call controller's action
            $controller->$action();
        }
        else
        {
            // we can throw an exception here too.
            Route::ErrorPage404();
        }
    }

    static function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}