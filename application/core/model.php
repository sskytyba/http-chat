<?php

namespace core;
use libs\DataBase;

class Model
{
    public function __construct()
    {
        DataBase::init($_SERVER['HTTP_HOST'], root, '', '467768' );
    } // block directly instantiating
    protected function getIdByLogin($login)
    {
        $query = "SELECT userID FROM users WHERE login = '$login'";
        $result = DataBase::get()->query($query);
        $row = mysqli_fetch_assoc($result);

        if (isset($row)) {
            return $row['userID'];
        } else {
            assert_options(ASSERT_WARNING, "DB hasn't such login");
            return null;
        }
    }
    protected function getLoginById($id)
    {
        $query = "SELECT login FROM users WHERE userID='$id'";
        $result = DataBase::get()->query($query);
        $row = mysqli_fetch_assoc($result);

        if(isset($row)){
            return $row['login'];
        } else {
            assert_options(ASSERT_WARNING, "DB hasn't such id");
            return null;
        }
    }
    public function updateOnline($login)
    {
        $id = $this->getIdByLogin($login);
        $query = DataBase::get()->query("SELECT * FROM onlineUsers WHERE id='$id'");
        $rows = mysqli_num_rows($query);
        if($rows == 0)
        {
            $query = "INSERT INTO onlineUsers (id) VALUES ('$id')";
            DataBase::get()->query($query);
        }
        else
        {
            $query = "UPDATE onlineUsers set time = now() WHERE id = '$id'";
            DataBase::get()->query($query);
        }
    }
    public function currentTime()
    {
        $query = DataBase::get()->query('select now()');
        $row = mysqli_fetch_assoc($query);
        return $row['now()'];
    }
    protected  function mysql_escape_mimic($str) {
        if(!empty($str) && is_string($str)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"),
                array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $str);
        }
        return $str;
    }
}