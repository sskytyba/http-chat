<?php

namespace core;

define('GLOBAL_APP_ROOT', $_SERVER["DOCUMENT_ROOT"]);

class View {

    //public $template_view; // here you can set default view.

    function generate($content_view, $template_view, $data = null, $extract = true)
    {

        if($extract && is_array($data)) {
            // we convert the array elements in variables.
            extract($data);
        }

        include GLOBAL_APP_ROOT.'/application/views/'.$template_view;
    }
} 