<?php

use core\Controller;

class ControllerLogout extends Controller
{

    function action_index()
    {
        session_start();
        // remove all session variables
        session_unset();
        // destroy the session
        session_destroy();

        if(isset($_COOKIE['pass'])) {
            unset($_COOKIE['pass']);
            unset($_COOKIE['login']);
            setcookie('pass', '', time() - 3600, '/');
            setcookie('login', '', time() - 3600, '/');
        }

        header("Location: /");
    }
}