<?php

use core\Controller;
use core\View;
use libs\User;
use libs\Messages;

class ControllerLogin extends Controller
{
    function __construct()
    {
        $this->model = new ModelLogin();
        $this->view = new View();
    }

    function action_index()
    {
        if (isset($_COOKIE['login']) && isset($_COOKIE['pass'])) {
            if($this->model->logIn(new User($_COOKIE['login'], $_COOKIE['pass'])))
            {
                session_start();
                $_SESSION['login'] = $_COOKIE['login'];
                $this->model->updateOnline($_SESSION['login']);
                header('Location: /chat/');
            }
            else
            {
                header('Location: /logout/');
            }
        }
        else if (isset($_POST['loginSub']))
        {
            if(!empty($_POST['login']) && !empty($_POST['pass']))
            {
                $user = new User($_POST['login'], $_POST['pass']);
                if($this->model->logIn($user))
                {
                    ControllerLogin::setCookies($user, isset($_POST['rememberMe']));
                    session_start();
                    $_SESSION['login'] = $user->getLogin();
                    $this->model->updateOnline($_SESSION['login']);
                    header('Location: /chat/');
                }
                else
                {
                    $this->view->generate('login_view.php', 'template_view.php', Messages::wrongAuthorizationData());
                }
            }
            else
            {
                $this->view->generate('login_view.php', 'template_view.php', Messages::wrongAuthorizationData());
            }
        }
        else {
            $this->view->generate('login_view.php', 'template_view.php');
        }
    }
    function setCookies(User $user, $rememberMe)
    {
        if ($rememberMe) {
            setcookie('login', $user->getLogin(), time()+60*60*24*365, '/'); //for a year
            setcookie('pass', $user->getPass(), time()+60*60*24*365, '/'); //for a year
        }
        else{
            setcookie('login', $user->getLogin(), false, '/');
            setcookie('pass', $user->getPass(), false, '/');
        }
    }
}