<?php

use core\Controller;
use core\View;

class ControllerChat extends Controller
{
    function __construct()
    {
        $this->model = new ModelChat();
        $this->view = new View();
    }

    function action_sendMessage()
    {
        if(isset($_SESSION['login']) && isset($_SESSION['friend']))
        {
            $this->model->sendMessage($_SESSION['login'], $_SESSION['friend'], $_POST['chatText']);
        }
    }

    function action_addFriend()
    {
        if(isset($_SESSION['login']) && isset($_POST['name']))
        {
            $this->model->addFriend($_SESSION['login'], $_POST['name']);
        }
    }
    function action_getLogins()
    {
        if(isset($_SESSION['login']) && isset($_POST['friend']))
        {
            $data = $this->model->getLoginsLike($_POST['friend']);
            $this->view->generate("loginsList_view.php", "empty_template_view.php", $data, false);
        }
    }
    function action_index()
    {
        if(isset($_SESSION['login'])) {
            $this->view->generate('chat_view.php', 'template_view.php');
        }
        else
        {
            header('Location: /');
        }
    }
    function action_update()
    {
        if(isset($_SESSION['login']) && isset($_SESSION['friend']))
        {
            $this->model->updateOnline($_SESSION['login']);
            $data = $this->model->getLastMessages($_SESSION['login'], $_SESSION['friend'], $_SESSION['lastMessageTime']);
            if(isset($data) && !empty($data))
            {
                $_SESSION['lastMessageTime'] = end($data)->getTime();
            }
            $this->view->generate("messages_view.php", "empty_template_view.php", $data, false);
        }
    }
    function action_updateUsers()
    {
        if(isset($_SESSION['login']))
        {
            $this->model->updateOnline($_SESSION['login']);
            $data = $this->model->getFriends($_SESSION['login']);
            $this->view->generate("friends_view.php", "empty_template_view.php", $data, false);
            $data = $this->model->getConfirmFriendship($_SESSION['login']);
            $this->view->generate("confirmFriendship_view.php", "empty_template_view.php", $data, false);
        }
    }
    function action_submitFriendship()
    {
        if(isset($_SESSION['login']) && isset($_SESSION['friend'])) {
            $this->model->acceptFriendship($_SESSION['login'], $_SESSION['friend'], isset($_POST['allow']));
        }

        header('Location: /');
    }
    function action_openChat()
    {
        if(isset($_SESSION['login']) && isset($_POST['friend'])) {

            $_SESSION['friend'] = $_POST['friend'];
            $data = $this->model->getMessages($_SESSION['login'], $_SESSION['friend']);

            if(isset($data) && !empty($data))
            {
                $_SESSION['lastMessageTime'] = end($data)->getTime();
            }
            else
            {
                $_SESSION['lastMessageTime'] = $this->model->currentTime();
            }

            $this->view->generate("messages_view.php", "empty_template_view.php", $data, false);
        }
    }
}