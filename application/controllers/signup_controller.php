<?php

use core\Controller;
use core\View;
use libs\User;
use libs\Messages;

class ControllerSignUp extends Controller
{
    function __construct()
    {
        $this->model = new ModelSignUp();
        $this->view = new View();
    }

    function action_index()
    {
        if(isset($_POST['sign-up'])) {
            if (!empty($_POST['login']) &&
                !empty($_POST['pass']) &&
                !empty($_POST['email'])) {

                $user = new User($_POST['login'], $_POST['pass']);
                $user->setEmail($_POST['email']);

                if($_POST['pass'] != $_POST['cpass'])
                {
                    $this->view->generate('signup_view.php', 'template_view.php', Messages::passwordsNotEqual());
                }
                else if (!$this->model->exist($user))
                {
                    $this->newUser($user);
                    header("Location: /");
                }
                else
                {
                    $this->view->generate('signup_view.php', 'template_view.php', Messages::userExist());
                }
            } else {
                $this->view->generate('signup_view.php', 'template_view.php', Messages::wrongSignUpData());
            }
        }
        else{
            $this->view->generate('signup_view.php', 'template_view.php');
        }

    }
    function newUser(User $user)
    {
        $this->model->addUser($user);
    }
}