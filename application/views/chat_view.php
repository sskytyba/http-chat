<link rel="stylesheet" type="text/css" href="../../css/page.css" />
<div id="chatContainer">

    <div id="chatTopBar" class="rounded">
        <span id="myName" class="name"><?php echo($_SESSION['login']); ?></span>
        <a href="/logout/" class="logoutButton rounded">Logout</a>
    </div>
    <div id="chatLineHolder"></div>
    <div id="addFriend" class="rounded">
        <form id="addForm">
            <input type="text" id="addText" name="name" class="rounded" maxlength="30" list="logins" onkeyup="onKey()" />
            <datalist id="logins">
            </datalist>
            <input type="submit" class="blueButton" value="Add" />
        </form>
    </div>
    <div id="chatUsers" class="rounded">


    </div>
    <div id="chatBottomBar" class="rounded">
        <div class="tip"></div>
        <form id="sendForm">
            <input type="text" id="chatText" name="chatText" class="rounded" maxlength="255" />
            <input type="submit" class="blueButton" value="Send" />
        </form>
    </div>

</div>

<script src="../../js/chatFunctions.js"></script>
