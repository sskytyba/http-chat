<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../../css/styles.css">
    <link rel="stylesheet" type="text/css" href="../../css/chat.css" />
    <title>MyChat</title>
</head>
<body>
    <?php include_once $_SERVER["DOCUMENT_ROOT"].'/application/views/'.$content_view; ?>
</body>
</html>