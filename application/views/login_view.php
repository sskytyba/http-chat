<div class="outer-wrapper">
    <div class="inner-wrapper">
        <div class="form">
            <div id="login">
                <form method="post" action="">
                    <?php
                    if(isset($errorMes))
                    {
                        echo "<label class='error-info'>".$errorMes."</label><br>";
                    }
                    ?>
                    <label>Login :</label>
                    <input id="name" name="login" placeholder="login" type="text">
                    <br>
                    <label>Password :</label>
                    <input name="pass" placeholder="**********" type="password">
                    <br>
                    <label>Remember Me </label>
                    <input type="checkbox" name="rememberMe" value="1">
                    <input id="button" type="submit" name="loginSub" value="Login">
                </form>
            </div>
        </div>
    </div>
</div>
<div id="top">
    <a href="/signup/">sign-up</a>
    <a href="/login/">login</a>
</div>