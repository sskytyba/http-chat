<?php
foreach($data as $key => $value)
{
    if($value->isMy())
    {
        echo("<div class='chat' id='fromUser'> "
            ."<span class='author'>".$value->getFrom()."</span>"
            ."<span class='text'><code>".$value->getMessage()."</code></span>"
            ."<span class='time'>".date_format(date_create($value->getTime()), 'H:i:s')."</span></div>");
    }
    else
    {
        echo("<div class='chat' id='fromFriend'> "
            ."<span class='author'>".$value->getFrom()."</span>"
            ."<span class='text'><code>".$value->getMessage()."</code></span>"
            ."<span class='time'>".date_format(date_create($value->getTime()), 'H:i:s')."</span></div>");
    }
}