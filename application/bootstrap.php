<?php
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';

require_once 'libs/database.php';
require_once 'libs/user.php';
require_once 'libs/messages.php';
require_once 'libs/message.php';


core\Route::start(); // router starts