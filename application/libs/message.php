<?php

namespace libs;

class Message
{
    private $from;
    private $to;
    private $message;
    private $time;
    private $my;

    public function __construct($from, $to, $message, $time, $my = false)
    {
        $this->from = $from;
        $this->to = $to;
        $this->message = $message;
        $this->time = $time;
        $this->my = $my;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function getTime()
    {
        return $this->time;
    }
    public function isMy()
    {
        return $this->my;
    }
}
