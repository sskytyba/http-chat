<?php

namespace libs;

class Messages
{
    static function wrongAuthorizationData()
    {
        return array( errorMes => "Wrong login or password.");
    }

    static function userExist()
    {
        return array( errorMes => "Such login is already registered.");
    }
    static function passwordsNotEqual()
    {
        return array( errorMes => "Confirm password and password are not equal.");
    }
    static function wrongSignUpData()
    {
        return Messages::wrongAuthorizationData();
    }
}