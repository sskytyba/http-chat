<?php

namespace libs;
define('TIMEZONE', 'Europe/Kiev');

class DataBase
{
    private static $instance; // stores the MySQLi instance

    private function __construct() { } // block directly instantiating
    private function __clone() { } // block cloning of the object

    public static function init($host, $user, $pass, $name)
    {
        // create the instance if it does not exist
        if(!isset(self::$instance)) {
            self::$instance = new \MySQLi('p:'.$host, $user, $pass, $name);
            if(self::$instance->connect_error) {
                throw new \Exception('MySQL connection failed: ' . self::$instance->connect_error);
            }
        }
    }
    public static function get() {
        // return the instance
        return self::$instance;
    }
}