<?php

namespace libs;

class User
{
    private $login;
    private $password;
    private $email;
    private $isOnline;
    private $toConfirm;

    public function __construct($login, $pass)
    {
        $this->login = $login;
        $this->password = $pass;
        $this->email = null;
        $this->isOnline = null;
        $this->toConfirm = null;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPass()
    {
        return $this->password;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setIsOnline($value)
    {
        $this->isOnline = $value;
    }
    public function isOnline()
    {
        return $this->isOnline;
    }
    public function setToConfirm($value)
    {
        $this->toConfirm = $value;
    }
    public function isToConfirm()
    {
        return $this->toConfirm;
    }
}
