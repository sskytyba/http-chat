<?php

use core\Model;
use libs\Message;
use libs\User;
use libs\DataBase;

class ModelChat extends Model
{
    public function acceptFriendship($user, $friend, $value)
    {
        $idTo = $this->getIdByLogin($user);
        $idFrom = $this->getIdByLogin($friend);

        if($idTo != $idFrom && $value == true)
        {
            $query = DataBase::get()->query("select * from confirmFriendship where fromID='$idFrom' AND toID = '$idTo'");
            $rows = mysqli_num_rows($query);
            if($rows == 1)
            {
                DataBase::get()->query("DELETE FROM confirmFriendship where fromID='$idFrom' AND toID = '$idTo'");
                DataBase::get()->query("INSERT INTO friends (userID,friendID) VALUES ('$idFrom','$idTo');");
                DataBase::get()->query("INSERT INTO friends (userID,friendID) VALUES ('$idTo','$idFrom');");
            }
        }
        else if($value == false) {
            $query = DataBase::get()->query("select * from confirmFriendship where fromID='$idFrom' AND toID = '$idTo'");
            $rows = mysqli_num_rows($query);
            if ($rows == 1) {
                DataBase::get()->query("DELETE FROM confirmFriendship where fromID='$idFrom' AND toID = '$idTo'");
            }
        }
    }
    public function addFriend($user, $friend)
    {
        $idTo = $this->getIdByLogin($friend);
        $idFrom = $this->getIdByLogin($user);
        if($idTo != $idFrom)
        {
            $query = DataBase::get()->query("select * from confirmFriendship where (fromID='$idFrom' AND toID = '$idTo') or
     (fromID='$idTo' AND toID = '$idFrom')");
            $rows = mysqli_num_rows($query);
            $query = DataBase::get()->query("select * from friends where (userID='$idFrom' AND friendID = '$idTo')");
            $rows1 = mysqli_num_rows($query);
            if($rows == 0 && $rows1==0)
            {
                DataBase::get()->query("INSERT INTO confirmFriendship (fromID,toID) VALUES ('$idFrom','$idTo');");
            }
        }
    }

    public function sendMessage($user, $friend, $message)
    {
        $idTo = $this->getIdByLogin($friend);
        $idFrom = $this->getIdByLogin($user);
        $message = $this->mysql_escape_mimic($message);
        if($idTo != $idFrom)
            DataBase::get()->query("INSERT INTO messages (fromID,toID,message) VALUES ('$idFrom','$idTo','$message')");
    }
    public function getFriends($login)
    {
        $id = $this->getIdByLogin($login);
        $query = "SELECT login, email, pass
                    FROM users,onlineUsers,friends
                    WHERE '$id' = friends.userID
                      AND friends.friendID = users.userID
                      AND friends.friendID = onlineUsers.id
                      AND time_to_sec(timediff(now(),onlineUsers.time)) < 10";
        $data = DataBase::get()->query($query);
        $result = array();
        while(isset($data) && $row = mysqli_fetch_assoc($data))
        {
            $user = new User($row['login'],$row['pass']);
            $user->setEmail($row['email']);
            $user->setIsOnline(true);
            array_push($result, $user);
        }
        $query = "SELECT login, email, pass
                    FROM users,onlineUsers,friends
                    WHERE '$id' = friends.userID
                      AND friends.friendID = users.userID
                      AND friends.friendID = onlineUsers.id
                      AND time_to_sec(timediff(now(),onlineUsers.time)) > 10";
        $data = DataBase::get()->query($query);
        while(isset($data) && $row = mysqli_fetch_assoc($data))
        {
            $user = new User($row['login'],$row['pass']);
            $user->setEmail($row['email']);
            $user->setIsOnline(false);
            array_push($result, $user);
        }
        return $result;
    }
    public function getConfirmFriendship($login)
    {
        $id = $this->getIdByLogin($login);
        $query = "SELECT login, email, pass
                    FROM users,confirmFriendship
                    WHERE '$id' = confirmFriendship.fromID
                      AND confirmFriendship.toID = users.userID";
        $data = DataBase::get()->query($query);
        $result = array();
        while(isset($data) && $row = mysqli_fetch_assoc($data))
        {
            $user = new User($row['login'],$row['pass']);
            $user->setEmail($row['email']);
            $user->setToConfirm(false);
            array_push($result, $user);
        }
        $query = "SELECT login, email, pass
                    FROM users,confirmFriendship
                    WHERE '$id' = confirmFriendship.toID
                      AND confirmFriendship.fromID = users.userID";
        $data = DataBase::get()->query($query);
        while(isset($data) && $row = mysqli_fetch_assoc($data))
        {
            $user = new User($row['login'],$row['pass']);
            $user->setEmail($row['email']);
            $user->setToConfirm(true);
            array_push($result, $user);
        }
        return $result;
    }
    public function getMessages($user, $friend)
    {
        $userID = $this->getIdByLogin($user);
        $friendID = $this->getIdByLogin($friend);

        $query = "SELECT * FROM messages WHERE (fromID = '$userID' AND toID = '$friendID')
                  OR (fromID = '$friendID' AND toID = '$userID') ORDER BY time ASC";
        $data = DataBase::get()->query($query);

        $result = array();
        while(isset($data) && !is_bool($data) && $row = mysqli_fetch_assoc($data))
        {
            $user = $this->getLoginById($row['fromID']);
            $friend = $this->getLoginById($row['toID']);
            array_push($result, new Message($user, $friend, $row['message'], $row['time'], $row['fromID'] == $userID));
        }
        return $result;
    }
    public function getLastMessages($user, $friend, $time)
    {
        $userID = $this->getIdByLogin($user);
        $friendID = $this->getIdByLogin($friend);

        $query = "SELECT * FROM messages WHERE ((fromID = '$userID' AND toID = '$friendID')
                  OR (fromID = '$friendID' AND toID = '$userID')) AND time > '$time' ORDER BY time ASC";

        $data = DataBase::get()->query($query);

        $result = array();
        while(isset($data) && !is_bool($data) && $row = mysqli_fetch_assoc($data))
        {
            $user = $this->getLoginById($row['fromID']);
            $friend = $this->getLoginById($row['toID']);
            array_push($result, new Message($user, $friend, $row['message'], $row['time'], $row['fromID'] == $userID));
        }
        return $result;
    }
    public function getLoginsLike($str)
    {
        $query = "SELECT login FROM users WHERE login LIKE '$str%'";
        $data = DataBase::get()->query($query);
        $result = array();
        while(isset($data) && !is_bool($data) && $row = mysqli_fetch_assoc($data))
        {
            array_push($result, $row['login']);
        }
        return $result;
    }
}