(function timer () {
    var timeBetweenCalls = 500; // Milliseconds
    var startTime = new Date().getTime();
    updateChat();

    setTimeout(timer, timeBetweenCalls - ((new Date().getTime() - startTime) % timeBetweenCalls));
})();
(function timer () {
    var timeBetweenCalls = 5000; // Milliseconds
    var startTime = new Date().getTime();
    updateUsers();

    setTimeout(timer, timeBetweenCalls - ((new Date().getTime() - startTime) % timeBetweenCalls));
})();

function updateChat()
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "/chat/update/", false);
    xmlHttp.send(null);

    var objDiv = document.getElementById("chatLineHolder");

    var toScroll = false;
    if((objDiv.scrollHeight - objDiv.scrollTop) < 500)
    {
        toScroll = true;
    }

    document.getElementById('chatLineHolder').innerHTML += xmlHttp.responseText;

    if(toScroll)
        objDiv.scrollTop = objDiv.scrollHeight;

}
function updateUsers()
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "/chat/updateUsers/", false);
    xmlHttp.send(null);
    document.getElementById('chatUsers').innerHTML = xmlHttp.responseText;
}
function openChat(value)
{
    var xmlHttp = new XMLHttpRequest();
    var params = "friend="+value;
    xmlHttp.open("POST", "/chat/openChat/", false);
    xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-length", params.length);
    xmlHttp.setRequestHeader("Connection", "close");

    xmlHttp.send(params);
    document.getElementById('chatLineHolder').innerHTML = xmlHttp.responseText;
}
function onKey()
{
    var xmlHttp = new XMLHttpRequest();
    var params = "friend="+document.getElementById('addText').value;
    xmlHttp.open("POST", "/chat/getLogins/", false);
    xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-length", params.length);
    xmlHttp.setRequestHeader("Connection", "close");
    xmlHttp.send(params);
    document.getElementById('logins').innerHTML = xmlHttp.responseText;

}
function showConfirm(value)
{
    var xmlHttp = new XMLHttpRequest();
    var params = "friend="+value;
    xmlHttp.open("POST", "/chat/openChat/", false);
    xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlHttp.setRequestHeader("Content-length", params.length);
    xmlHttp.setRequestHeader("Connection", "close");
    xmlHttp.send(params);
    document.getElementById('chatLineHolder').innerHTML =
        "Do you agree for a friendship with " + value + "?" +
        "<form method='post' action='/chat/submitFriendship/'>" +
        "<input type='submit' class='blueButton' name='allow' value='Allow' />"+
        "<input type='submit' class='blueButton' name='deny' value='Deny' />"+
        "</form>";
}
window.addEventListener("load", function () {
    function sendData() {
        var XHR = new XMLHttpRequest();

        var FD  = new FormData(form);

        XHR.addEventListener("load", function(event) {
        });

        XHR.addEventListener("error", function(event) {
            alert('Oups! Something goes wrong.');
        });

        XHR.open("POST", "/chat/sendMessage/", true);

        XHR.send(FD);
    }
    function addFriend() {
        var XHR = new XMLHttpRequest();

        var FD  = new FormData(form2);

        XHR.addEventListener("load", function(event) {
        });

        XHR.addEventListener("error", function(event) {
            alert('Oups! Something goes wrong.');
        });

        XHR.open("POST", "/chat/addFriend/", true);

        XHR.send(FD);
    }

    var form = document.getElementById("sendForm");

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        sendData();
        document.getElementById('chatText').value = "";
    });

    var form2 = document.getElementById("addForm");

    form2.addEventListener("submit", function (event) {
        event.preventDefault();
        addFriend();
        document.getElementById('addText').value = "";
    });
});